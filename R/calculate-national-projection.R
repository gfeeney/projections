calculate.national.projection <- function(npi) {
  # Calculate national projection from list of initialized pframes
  # National projection is llst of pframes, one for each projection cycle
  np <- npi
  pframe <- np[[1]]
  for (i in 1:length(pcycles)) {
    if (i > 1) {
      rowsASD <- c(paste0("f", getAG(width, oeg)), paste0("m", getAG(width, oeg)))
      pframe[rowsASD, "ASDin"] <- np[[i - 1]][rowsASD, "ASDout"]
    }
    pframe <- calculate.DeathsIP(pframe)
    pframe <- calculate.SurvivorsIP(pframe)
    pframe <- calculate.Births(pframe)
    pframe <- calculate.Births2(pframe)
    pframe <- calculate.DeathsB(pframe)
    pframe <- calculate.SurvivorsB(pframe)
    pframe <- calculate.ASDout(pframe)
    pframe <- calculate.totals(pframe)
    np[[i]] <- pframe
  }
  return(np)
}

initialize.national.projection <- function(pcycles, width, oeg, rag, nLx, ASBR, NIMR, BaseASD) {
  # Arg: 
  area <- colnames(BaseASD)[1]
  npi <- vector(mode = "list", length = length(pcycles))
  names(npi) <- pcycles
  for (i in 1:length(pcycles)) {
    npi[[i]] <- initialize.pframe(area, pcycles[[i]], width, oeg, rag, nLx, ASBR, NIMR)
  }
  npi[[1]][names(BaseASD[, area]), "ASDin"] <- BaseASD[, area]
  
  #np[[1]] <- pframe
  return(npi)
}

initialize.pframe <- function(area, pcycle, width, oeg, rag, nLx, ASBR, NIMR) { 
  # Creates a pframe for a given projection cycle (pcycle) and area, national
  # or subnational, containing the nLx, ASBR, and NIMR values from the input
  # .cvs files. This function does NOT enter the BaseASD. This is done by the
  # initialize.national.projection function, which identifies the first pcycle.
  # Knowing whether the pcycle argument to this function is first woule require
  # an additional input.
  pframe <- construct.pframe(width, oeg, rag)

  pframe[rownames(NIMR[[1]]), "NIMR"] <- NIMR[[area]][, pcycle]
  
  ASBR.rows <- rownames(ASBR[[1]])
  ASBR.rows[1] <- "fBirths"
  pframe[ASBR.rows, "ASBR"] <- ASBR[[area]][, pcycle]
  
  nLx.rows <- rownames(nLx[[1]])
  n <- length(nLx.rows) / 2
  nLx.rows[(n - 1) + c(0, n)] <- c("f95+", "m95+")  # Need to assign f95+ and m95+ here
  pframe[nLx.rows, "nLx"] <- nLx[[area]][, pcycle]
  pframe[c("fBirths", "mBirths"), "nLx"] <- 5
  
  return(pframe)
}

get.width <- function(pframe) {
  # Return of the width of age groups in a pframe
  x <- strsplit(rownames(pframe)[2], split = "")[[1]]
  width <- as.numeric(x[4]) + 1
  return(width)
}

get.oeg <- function(pframe) {
  x <- rownames(pframe)
  n <- (length(x)/2) - 3
  oeg <- (n - 1) * width
  return(oeg)
}

get.rag <- function(pframe) {
  x <- pframe[paste0("f", getAG(width, oeg)), "Births"]
  rag <- which(!is.na(x))
  return(rag)
}

calculate.DeathsIP <- function(pframe) {
  # Calculate deaths of initial population
  rows <- 2:(1 + get.oeg(pframe)/get.width(pframe))
  calculate <- function(pframe, rows) {
    SR <- pframe[rows + 1, "nLx"]/pframe[rows , "nLx"]
    pframe[rows, "Deaths"] <- (1 - SR) * pframe[rows, "ASDin"]
    row <- rows[length(rows)] + 1
    SR <- pframe[row + 1, "nLx"]/sum(pframe[row + 0:1, "nLx"])
    Deaths <- (1 - SR) * pframe[row, "ASDin"]
    pframe[row, "Deaths"] <- Deaths
    return(pframe)
  }
  pframe <- calculate(pframe, rows)
  rows <- rows + dim(pframe)[1]/2
  pframe <- calculate(pframe, rows)
  return(pframe)
}

calculate.SurvivorsIP <- function(pframe) {
  n <- dim(pframe)[1]/2  - 5
  rows <- 2:(n + 1)
  calculate <- function(pframe, rows) {
    Survivors <- pframe[rows, "ASDin"] - pframe[rows, "Deaths"]
    pframe[rows + 1, "Survivors"] <- Survivors
    rows <- rows[length(rows)] + 1:2
    Survivors <- sum(pframe[rows, "ASDin"]) - sum(pframe[rows, "Deaths"])
    pframe[rows[2], "Survivors"] <- Survivors 
    return(pframe) 
  }
  pframe <- calculate(pframe, rows)
  rows <- rows + dim(pframe)[1]/2
  pframe <- calculate(pframe, rows)
  return(pframe)
}

calculate.Births <- function(pframe) {
  # Calculate total births by age of mother
  rowsB <- 1 + get.rag(pframe)
  PYL <- width * (pframe[rowsB, "ASDin"] + pframe[rowsB, "Survivors"]) / 2
  Births <- pframe[rowsB, "ASBR"] * PYL
  pframe[names(Births), "Births"] <- Births
  return(pframe)
}

calculate.Births2 <- function(pframe) {
  # Calculate total female births annd total male births
  rowsB <- 1 + get.rag(pframe)
  BirthsT <- sum(pframe[rowsB, "Births"])
  srb <- pframe["fBirths", "ASBR"]
  BirthsF <- BirthsT * 1/(1 + srb)
  pframe["fTotal", "Births"] <- BirthsT
  pframe["fBirths", "ASDin"] <- BirthsF
  pframe["mBirths", "ASDin"] <- BirthsT - BirthsF
  return(pframe)
}

calculate.DeathsB <- function(pframe) {
  # Calculate deaths of births
  row <- 1
  calculate <- function(pframe, row) {
    SR <- pframe[row + 1, "nLx"]/pframe[row, "nLx"]
    pframe[row, "Deaths"] <- (1 - SR) * pframe[row, "ASDin"]
    return(pframe)
  }
  pframe <- calculate(pframe, row)
  row <- row + dim(pframe)[1]/2
  pframe <- calculate(pframe, row)
  return(pframe)
}

calculate.SurvivorsB <- function(pframe) {
  # Calculate survivors of births
  row <- 1
  calculate <- function(pframe, row) {
    Survivors <- pframe[row, "ASDin"] - pframe[row, "Deaths"]
    pframe[row + 1, "Survivors"] <- Survivors
    return(pframe)
  }
  pframe <- calculate(pframe, row)
  row <- row + dim(pframe)[1]/2
  pframe <- calculate(pframe, row)
  return(pframe)
}

calculate.ASDout <- function(pframe) {
  # Calculate ASDout
  width <- get.width(pframe)
  oeg <- get.oeg(pframe)
  rows <- c(paste0("f", getAG(width, oeg)), paste0("m", getAG(width, oeg)))
  pframe[rows, "ASDout"] <- pframe[rows, "Survivors"] + pframe[rows, "NIM"]
  return(pframe)
}

calculate.totals <- function(pframe) {
  # Calculate entries in fTotal and mTotal rows
  width <- get.width(pframe)
  oeg <- get.oeg(pframe)
  rowsASD <- c(paste0("f", getAG(width, oeg)), paste0("m", getAG(width, oeg)))
  rowsnLx <- c(paste0("f", getAG(width, oeg + 5)), paste0("m", getAG(width, oeg + 5)))
  
  n <- length(rowsASD)/2
  pframe["fTotal", "ASDin"] <- sum(pframe[rowsASD[1:n], "ASDin"])
  pframe["mTotal", "ASDin"] <- sum(pframe[rowsASD[n + 1:n], "ASDin"])
  
  rows <- 2:(dim(pframe)[1]/2 - 2)
  pframe["fTotal", "nLx"] <- sum(pframe[rows, "nLx"])
  rows <- rows + dim(pframe)[1]/2
  pframe["mTotal", "nLx"] <- sum(pframe[rows, "nLx"])
  
  n <- length(rowsASD)/2
  pframe["fTotal", "Deaths"] <- sum(pframe[c("fBirths", rowsASD[1:n]), "Deaths"])
  pframe["mTotal", "Deaths"] <- sum(pframe[c("mBirths", rowsASD[n + 1:n]), "Deaths"])
  
  pframe["fTotal", "ASBR"] <- 5 * sum(pframe[rowsASD[rag], "ASBR"])
  
  n <- length(rowsASD)/2
  pframe["fTotal", "Survivors"] <- sum(pframe[rowsASD[1:n], "Survivors"])
  pframe["mTotal", "Survivors"] <- sum(pframe[rowsASD[n + 1:n], "Survivors"])
  
  n <- length(rowsASD)/2
  pframe["fTotal", "NIMR"] <- sum(pframe[rowsASD[1:n], "NIMR"])
  pframe["mTotal", "NIMR"] <- sum(pframe[rowsASD[n + 1:n], "NIMR"])
  
  n <- length(rowsASD)/2
  pframe["fTotal", "NIM"] <- sum(pframe[rowsASD[1:n], "NIM"])
  pframe["mTotal", "NIM"] <- sum(pframe[rowsASD[n + 1:n], "NIM"])

  n <- length(rowsASD)/2
  pframe["fTotal", "ASDout"] <- sum(pframe[rowsASD[1:n], "ASDout"])
  pframe["mTotal", "ASDout"] <- sum(pframe[rowsASD[n + 1:n], "ASDout"])
  
  return(pframe)
}



# construct.pframe <- function(width, oeg, rag = 4:10) {
#   # Construct a pframe from width and oeg of input age distributions
#   # Default argument for reproductive age groups gives index values
#   # of the standard 7 age groups, 15-19, 20-24, . . ., 45-49
#   x <- c("Births", getAG(width, oeg), paste0(oeg + 5, "+"), "Total")
#   n <- length(x)
#   rnames <- c(paste0("f", x), paste0("m", x))
#   cnames <- c("ASDin", "nLx", "Deaths", "ASBR", "Births", "Survivors", "NIMR", "NIM", "ASDout")
#   pframe <- matrix(0, nrow = length(rnames), ncol = length(cnames))
#   rownames(pframe) <- rnames
#   colnames(pframe) <- cnames
#   pframe[c(1, n), c("Births", "Survivors", "NIMR", "NIM", "ASDout")] <- NA
#   pframe[paste0("f", getAG(width, oeg)[-rag]), c("ASBR", "Births")] <- NA
#   pframe[paste0("m", getAG(width, oeg)), c("ASBR", "Births")] <- NA
#   pframe[(length(x) - 1) + c(0, n), cnames[-2]] <- NA
#   return(pframe)
#}

# construct.pframeOLD <- function(agegroups) {
#   # Construct 0 iniitalized pframe matrix with row and column names.
#   # Argument 'agegroups' is awkward, but do NOT want to change this now,
#   # too many ramifications.
#   # browser()
#   agegroups2rownames <- function(x) {
#     oeg <- as.numeric(gsub("+", "", x[length(x)], fixed = TRUE))
#     width <- 1 + diff(as.numeric(strsplit(x[length(x) - 1], split = "-")[[1]]))
#     n <- length(x)
#   #  x[n] <- paste0(x[n], "/", paste0(oeg, "-", oeg + width - 1))
#     x <- c("Births", x, paste0(oeg + width, "+"), "Total")
#     c(paste0("f", x), paste0("m", x))
#   }
#   rnames <- agegroups2rownames(agegroups)
#   cnames <- c("ASDin", "nLx", "Deaths", "ASBR", "Births", "Survivors", 
#               "NIMR", "NIM", "ASDout")
#   pframe <- matrix(0, nrow = length(rnames), ncol = length(cnames))
#   rownames(pframe) <- rnames
#   colnames(pframe) <- cnames
#   return(pframe)
# }


# calculate.DIP.OLD <- function(pframe) {
#   # Calculate deaths of initial population
#   calculate <- function(pframe, sex) {
#     x <- pframe[substr(rownames(pframe), 1, 1) == sex, ]
#     n <- dim(x)[1] - 3   # Number of age groups in ASDin 
#     ASDin <- x[2:(n + 1), "ASDin"]  # Last age group is open-ended (ignore label)
#     nLx   <- x[2:(n + 2), "nLx"]    # Last age group is open-ended (ignore label) 
#     Deaths <- ASDin * (1 - nLx[-1] / nLx[-length(nLx)])
#     Deaths[n] <- ASDin[n] * (1 - nLx[n + 1] / sum(nLx[n:(n + 1)]))  # Overwrite oeg
#     x[2:(n + 1), "Deaths"] <- Deaths
#     return(x)
#   }
#   pframe <- rbind(calculate(pframe, "f"), calculate(pframe, "m"))
#   return(pframe)
# }


# calculate.DB <- function(pframe) {
#   # Calculate deaths of female births and deaths of male births
#   calculate <- function(pframe, sex) {
#     x <- pframe[substr(rownames(pframe), 1, 1) == sex, ]
#     x[1, "Deaths"] <- x[1, "ASDin"] * (1 - x[2, "nLx"] / x[1, "nLx"])
#     return(x)
#   }
#   pframe <- rbind(calculate(pframe, "f"), calculate(pframe, "m"))
#   return(pframe)
# }

# calculate.SIP.OLD <- function(pframe) {
#   # Calculate survivors of initial population
#   n <- (dim(pframe)[1] / 2) - 3
#   rows <- c(1:(n + 1), 1:(n + 1) + n + 3)
#   ASDin <- pframe[rows, "ASDin"]
#   Deaths <- pframe[rows, "Deaths"]
#   pframe[rows + 1, "Survivors"] <- ASDin - Deaths  # Deaths to (oeg + 5)+
#   rows <- n + 1:2                             # Collapse to oeg+
#   pframe[rows, "Survivors"] <- c(sum(pframe[rows, "Survivors"]), 0)
#   pframe[rows + n + 3, "Survivors"] <- c(sum(pframe[rows + n + 3, "Survivors"]), 0)
#   return(pframe)
# }